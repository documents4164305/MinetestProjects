# Minetest Projects

A gitlab project designed to help new players join Minetest.

# What is Minetest?

Minetest is a free Open Source game that is similar to Minecraft, free from the censorship and control of Microsoft. It is a cross-platform compatible game that includes the easy installation of mods.

# How can I install Minetest?

Installation instructions are available within the Minetest Projects subdirectories for Linux, Android and Windows, and does not require complicated 'build from source' setups.

- Linux Installation Instructions can be found [here](https://gitlab.com/documents4164305/MinetestProjects/-/blob/main/InstallationInstructions/LinuxInstallation.md).
- Windows Installation Instructions can be found [here](https://gitlab.com/documents4164305/MinetestProjects/-/blob/main/InstallationInstructions/WindowsInstallation.md).
- Android Installation Instructions can be found [here](https://gitlab.com/documents4164305/MinetestProjects/-/blob/main/InstallationInstructions/AndroidInstallation.md).
