To install Minetest on Linux, it varies based on which distro you're using.

If you are using a Debian-based distribution (such as Ubuntu or Linux Mint), run `sudo apt update`, then `sudo apt install minetest`.

If you are using an Arch-based distribution (such as Manjaro), run `sudo pacman -Syu`, then `sudo pacman -S minetest`.

If you are using a Gentoo-based distribution, run `sudo emaint -a sync`, then `sudo emerge minetest`.