If you are using an Android phone, [the Google Play store has an official application for Minetest.](https://play.google.com/store/apps/details?id=net.minetest.minetest)

It is also [available on the F-Droid repository](https://f-droid.org/packages/net.minetest.minetest/), if you are using a device that is rooted and degoogled.
